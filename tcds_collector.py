#!/usr/bin/env python3
import datetime
import json
import time
import urllib.request
import sys
import utils
from urllib.error import URLError, HTTPError
from prometheus_client import start_http_server
import dashboards
from dotenv import load_dotenv
import os
import signal
import logging

logging.basicConfig(filename="/tmp/cpm_log.txt",level=logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %H:%M:%S')
interrupted = False


load_dotenv()

url = os.getenv("SOURCE_URL")
grafana_url = os.getenv("GRAFANA_URL")
dashboardUID = os.getenv("TCDS_CONTROL_DASHUID")
poll_time_seconds = 1
token = os.getenv("TOKEN")
last_run_number = -1
flag = 0
times_not_new_snapshot = 1

def signal_handler(sig, _):
    logging.warning(f"Recieved signal {signal.Signals(sig).name}({sig}).")
    global interrupted
    interrupted = True


def work_snapshot(snapshot, partitions_dict, partitions_index_dict,pm_counters_dict):
    dashboards.gauge_not_new_snapshot.set(0)

    global last_run_number
    global not_new_snapshot
    not_new_snapshot = 1
    run_number = snapshot["Run configuration"]["Run number"]
    
    if run_number != last_run_number:

        last_run_number = run_number
        utils.make_post_request(utils.create_encoded_body("POST", dashboardUID, 2, "new_run",
                                "New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", dashboardUID, 3, "new_run",
                                "New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", dashboardUID, 41, "new_run",
                                "New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)

    # partitions_dict = (utils.create_partitions_dict(snapshot))[0]
    # partitions_index_dict = (utils.create_partitions_dict(snaps  hot))[1]
    date_format = datetime.datetime.strptime(
            snapshot["Application state"]["Latest monitoring update time"].split(" UTC")[0], "%Y-%m-%d %H:%M:%S")
    unix_time = datetime.datetime.timestamp(date_format)
    dashboards.gauge_latest_monitoring_update_time.set(unix_time)
    dashboards.gauge_latest_monitoring_update_duration.set(float(snapshot["Application state"]["Latest monitoring update duration (s)"]))

    
    dashboards.gauge_total_L1As_in_current_run.set(utils.get_trigger_counter_value(snapshot,"Total # L1As in the current run"))
    
    dashboards.gauge_nibbles_rates_total_trigger_rate.set(utils.get_nibbles_rate(snapshot,"Total trigger rate"))
    dashboards.gauge_nibbles_rates_physics_trigger.set(utils.get_nibbles_rate(snapshot,"Physics trigger"))
    dashboards.gauge_nibbles_rates_sequence_calibration_trigger.set(utils.get_nibbles_rate(snapshot,"Sequence/calibration trigger"))
    dashboards.gauge_nibbles_rates_random_trigger.set(utils.get_nibbles_rate(snapshot,"Random trigger"))
 
    dashboards.gauge_nibbles_deadtimes_total.set(utils.get_nibbles_deadtimes(snapshot,"Total deadtime"))

    dashboards.gauge_nibbles_deadtime_counter.labels("tts").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to TTS"))
    dashboards.gauge_nibbles_deadtime_counter.labels("trigger_rules").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to trigger rules"))
    dashboards.gauge_nibbles_deadtime_counter.labels("bunch_mask").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to the bunch-mask trigger veto"))
    dashboards.gauge_nibbles_deadtime_counter.labels("re_tri").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to the ReTri trigger veto"))
    dashboards.gauge_nibbles_deadtime_counter.labels("pm_apve").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to the PM APVE trigger veto"))
    dashboards.gauge_nibbles_deadtime_counter.labels("daq_backpressure").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to DAQ backpressure to the PM"))
    dashboards.gauge_nibbles_deadtime_counter.labels("calibration_sequence").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to the calibration sequence"))
    dashboards.gauge_nibbles_deadtime_counter.labels("software_pauses").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to software pauses"))
    dashboards.gauge_nibbles_deadtime_counter.labels("firmware_pauses").set(utils.get_nibbles_deadtimes(snapshot,"Deadtime due to firmware pauses"))



    dashboards.gauge_latest_section_lumi_section_number.set(utils.get_cpm_latest_section_value(snapshot,"Lumi-section number"))
    dashboards.gauge_latest_section_number_of_nibbles.set(utils.get_cpm_latest_section_value(snapshot,"Number of nibbles"))
    
    
    
        
    lpms_list = utils.get_lpms(snapshot)
    # all_partitions_names = []
    # for lpm in lpms_list:
    #     all_partitions_names.extend(partitions_dict["tts-counters"][lpm].keys())

    # print(all_partitions_names)

    for lpm in lpms_list:
       

        partitions_names = list((partitions_dict["tts-counters"][lpm]).keys())
        if len(partitions_names) == 0:

            continue
        else:
            for i in range(0, len(partitions_names)):

                new_time_counters =  utils.get_tts_time_and_transition_counter_lpm_all_values(snapshot,lpm,partitions_names[i],partitions_index_dict[partitions_names[i]])[0]
                new_transition_counters = utils.get_tts_time_and_transition_counter_lpm_all_values(snapshot,lpm,partitions_names[i],partitions_index_dict[partitions_names[i]])[1]
                
                s = lpm.replace("time","transition")
                if ((new_time_counters)) == "not_in_the_run":
                    #  FUTURE IMPORVEMENT for the next time without a beam, break this logic to 2/3 functions
                    # Ready
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["ready"] = -1
                    partitions_dict["tts-counters"][s][partitions_names[i]]["ready"] = -1
                    dashboards.gauge_acc_tts_counter_ready.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_ready.labels(partitions_names[i]).set(-1)

                    dashboards.gauge_acc_tts_transition_counter_ready.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_ready.labels(partitions_names[i]).set(-1)

                    # Warning

                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning"] = -1
                    partitions_dict["tts-counters"][s][partitions_names[i]]["warning"] = -1

                    dashboards.gauge_acc_tts_counter_warning.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_warning.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_acc_tts_counter_ready_and_warning.labels(partitions_names[i]).set(-1)

                    dashboards.gauge_acc_tts_transition_counter_warning.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_warning.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_acc_tts_transition_counter_ready_and_warning.labels(partitions_names[i]).set(-1)

                    # Warning back
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning_back"] = -1
                    partitions_dict["tts-counters"][s][partitions_names[i]]["warning_back"] = -1

                    dashboards.gauge_acc_tts_counter_warning_back.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_warning_back.labels(partitions_names[i]).set(-1)

                    dashboards.gauge_acc_tts_transition_counter_warning_back.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_warning_back.labels(partitions_names[i]).set(-1)
                    # out of sync
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["out_of_sync"] = -1
                    partitions_dict["tts-counters"][s][partitions_names[i]]["out_of_sync"] = -1

                    dashboards.gauge_acc_tts_counter_out_of_sync.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_out_of_sync.labels(partitions_names[i]).set(-1)

                    dashboards.gauge_acc_tts_transition_counter_out_of_sync.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_out_of_sync.labels(partitions_names[i]).set(-1)

                    #busy

                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["busy"] = -1
                    partitions_dict["tts-counters"][s][partitions_names[i]]["busy"] = -1

                    dashboards.gauge_acc_tts_counter_busy.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_busy.labels(partitions_names[i]).set(-1)
                    
                    dashboards.gauge_acc_tts_transition_counter_busy.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_busy.labels(partitions_names[i]).set(-1)
                    
                    # exception
                    dashboards.gauge_acc_tts_counter_exception.labels(partitions_names[i]).set(-1)
                    dashboards.guage_inc_acc_tts_counter_exception.labels(partitions_names[i]).set(-1)

                    dashboards.gauge_acc_tts_transition_counter_exception.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_exception.labels(partitions_names[i]).set(-1)

                    # total
                    dashboards.gauge_acc_tts_counter_total.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_counter_total.labels(partitions_names[i]).set(-1)
                    
                    dashboards.gauge_acc_tts_transition_counter_total.labels(partitions_names[i]).set(-1)
                    dashboards.gauge_inc_acc_tts_transition_counter_total.labels(partitions_names[i]).set(-1)

                else:
                    curr_time_counters_total = 0
                    curr_transition_counters_total = 0
                    # retrieving ready and setting to prometheus and my structure

                    # Time counters
                    prev_time_ready = partitions_dict["tts-counters"][lpm][partitions_names[i]]["ready"]
                    curr_time_ready = new_time_counters["ready"]
                    increment_time_ready = utils.calculate_increment(prev_time_ready,curr_time_ready)
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["ready"] = curr_time_ready
                    dashboards.gauge_acc_tts_counter_ready.labels(partitions_names[i]).set(curr_time_ready)
                    dashboards.gauge_inc_acc_tts_counter_ready.labels(partitions_names[i]).set(increment_time_ready)
                    curr_time_counters_total = curr_time_counters_total + curr_time_ready

                    # Transition counters
                   
                    prev_transition_ready = partitions_dict["tts-counters"][s][partitions_names[i]]["ready"]
                    curr_transition_ready = new_transition_counters["ready"]
                    increment_transition_ready = utils.calculate_increment(prev_transition_ready,curr_transition_ready)
                    partitions_dict["tts-counters"][s][partitions_names[i]]["ready"] = curr_transition_ready
                    dashboards.gauge_acc_tts_transition_counter_ready.labels(partitions_names[i]).set(curr_transition_ready)
                    dashboards.gauge_inc_acc_tts_transition_counter_ready.labels(partitions_names[i]).set(increment_transition_ready)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_ready
                    

                    # retrieving warning and setting to prometheus and my structure
                    # Time counters
                    prev_time_warning = partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning"]
                    curr_time_warning = new_time_counters["warning"]
                    increment_time_warning = utils.calculate_increment(prev_time_warning,curr_time_warning)    
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning"] = curr_time_warning
                    dashboards.gauge_acc_tts_counter_warning.labels(partitions_names[i]).set(curr_time_warning)
                    dashboards.gauge_inc_acc_tts_counter_warning.labels(partitions_names[i]).set(increment_time_warning)
                    curr_time_counters_total = curr_time_counters_total + curr_time_warning

                    # Transition counters
                    prev_transition_warning = partitions_dict["tts-counters"][s][partitions_names[i]]["warning"]
                    curr_transition_warning = new_transition_counters["warning"]
                    increment_transition_warning = utils.calculate_increment(prev_transition_warning,curr_transition_warning)    
                    partitions_dict["tts-counters"][s][partitions_names[i]]["warning"] = curr_transition_warning
                    dashboards.gauge_acc_tts_transition_counter_warning.labels(partitions_names[i]).set(curr_transition_warning)
                    dashboards.gauge_inc_acc_tts_transition_counter_warning.labels(partitions_names[i]).set(increment_transition_warning)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_warning


                    # warning back
                    # time counters
                    prev_time_warning_back = partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning_back"]
                    curr_time_warning_back = new_time_counters["warning_back"]
                    increment_time_warning_back = utils.calculate_increment(prev_time_warning_back,curr_time_warning_back)
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["warning_back"] = curr_time_warning_back
                    dashboards.gauge_acc_tts_counter_warning_back.labels(partitions_names[i]).set(curr_time_warning_back)
                    dashboards.gauge_inc_acc_tts_counter_warning_back.labels(partitions_names[i]).set(increment_time_warning_back)
                    curr_time_counters_total = curr_time_counters_total + curr_time_warning_back

                    # transition
                    prev_transition_warning_back = partitions_dict["tts-counters"][s][partitions_names[i]]["warning_back"]
                    curr_transition_warning_back = new_transition_counters["warning_back"]
                    increment_transition_warning_back = utils.calculate_increment(prev_transition_warning_back,curr_transition_warning_back)
                    partitions_dict["tts-counters"][s][partitions_names[i]]["warning_back"] = curr_transition_warning_back
                    dashboards.gauge_acc_tts_transition_counter_warning_back.labels(partitions_names[i]).set(curr_transition_warning_back)
                    dashboards.gauge_inc_acc_tts_transition_counter_warning_back.labels(partitions_names[i]).set(increment_transition_warning_back)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_warning_back

                    # retrieving out of sync and setting to prometheus and my structure
                    # time counters
                    prev_time_out_of_sync = partitions_dict["tts-counters"][lpm][partitions_names[i]]["out_of_sync"]
                    curr_time_out_of_sync = new_time_counters["out_of_sync"]
                    increment_time_out_of_sync = utils.calculate_increment(prev_time_out_of_sync,curr_time_out_of_sync)
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["out_of_sync"] = curr_time_out_of_sync
                    dashboards.gauge_acc_tts_counter_out_of_sync.labels(partitions_names[i]).set(curr_time_out_of_sync)
                    dashboards.gauge_inc_acc_tts_counter_out_of_sync.labels(partitions_names[i]).set(increment_time_out_of_sync)
                    curr_time_counters_total = curr_time_counters_total + curr_time_out_of_sync
                    
                    # transition coutners
                    prev_transition_out_of_sync = partitions_dict["tts-counters"][s][partitions_names[i]]["out_of_sync"]
                    curr_transition_out_of_sync = new_transition_counters["out_of_sync"]
                    increment_transition_out_of_sync = utils.calculate_increment(prev_transition_out_of_sync,curr_transition_out_of_sync)
                    partitions_dict["tts-counters"][s][partitions_names[i]]["out_of_sync"] = curr_transition_out_of_sync
                    dashboards.gauge_acc_tts_transition_counter_out_of_sync.labels(partitions_names[i]).set(curr_transition_out_of_sync)
                    dashboards.gauge_inc_acc_tts_transition_counter_out_of_sync.labels(partitions_names[i]).set(increment_transition_out_of_sync)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_out_of_sync

                    # # retrieving busy and setting to prometheus and my structure
                    # time counters
                    prev_time_busy = partitions_dict["tts-counters"][lpm][partitions_names[i]]["busy"]
                    curr_time_busy = new_time_counters["busy"]
                    increment_time_busy = utils.calculate_increment(prev_time_busy,curr_time_busy)
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["busy"] = curr_time_busy
                    dashboards.gauge_acc_tts_counter_busy.labels(partitions_names[i]).set(curr_time_busy)
                    dashboards.gauge_inc_acc_tts_counter_busy.labels(partitions_names[i]).set(increment_time_busy)
                    curr_time_counters_total = curr_time_counters_total + curr_time_busy

                    # transition counters
                    prev_transition_busy = partitions_dict["tts-counters"][s][partitions_names[i]]["busy"]
                    curr_transition_busy = new_transition_counters["busy"]
                    increment_transition_busy = utils.calculate_increment(prev_transition_busy,curr_transition_busy)
                    partitions_dict["tts-counters"][s][partitions_names[i]]["busy"] = curr_transition_busy
                    dashboards.gauge_acc_tts_transition_counter_busy.labels(partitions_names[i]).set(curr_transition_busy)
                    dashboards.gauge_inc_acc_tts_transition_counter_busy.labels(partitions_names[i]).set(increment_transition_busy)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_busy
                    # # retrieving private requests and setting to prometheus and my structure
                    # time
                    prev_time_private_requests = partitions_dict["tts-counters"][lpm][partitions_names[i]]["requests"]
                    curr_time_private_requests = new_time_counters["requests"]
                    increment_time_private_requests = utils.calculate_increment(prev_time_private_requests,curr_time_private_requests)
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["requests"] = curr_time_private_requests
                    dashboards.gauge_acc_tts_counter_private_requests.labels(partitions_names[i]).set(curr_time_private_requests)
                    dashboards.gauge_inc_acc_tts_counter_private_requests.labels(partitions_names[i]).set(increment_time_private_requests)
                    curr_time_counters_total = curr_time_counters_total + curr_time_private_requests

                     # transition
                    prev_transition_private_requests = partitions_dict["tts-counters"][s][partitions_names[i]]["requests"]
                    curr_transition_private_requests = new_transition_counters["requests"]
                    increment_transition_private_requests = utils.calculate_increment(prev_transition_private_requests,curr_transition_private_requests)
                    partitions_dict["tts-counters"][s][partitions_names[i]]["requests"] = curr_transition_private_requests
                    dashboards.gauge_acc_tts_transition_counter_private_requests.labels(partitions_names[i]).set(curr_transition_private_requests)
                    dashboards.gauge_inc_acc_tts_transition_counter_private_requests.labels(partitions_names[i]).set(increment_transition_private_requests)
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_private_requests
                    
                    # # retrieving error ,disconnected, sum of invalid, and link lost that are my acc_excpetion metri
                    # # and setting to prometheus and my structure
                    
                    prev_time_error = partitions_dict["tts-counters"][lpm][partitions_names[i]]["error"]
                    prev_time_sum_of_invalid = partitions_dict["tts-counters"][lpm][partitions_names[i]]["sum_of_invalid"]
                    prev_time_disconnected = partitions_dict["tts-counters"][lpm][partitions_names[i]]["disconnected"]
                    prev_time_sum_of_link_lost = partitions_dict["tts-counters"][lpm][partitions_names[i]]["sum_of_link_lost"]
                    prev_time_exception = prev_time_error + prev_time_disconnected + prev_time_sum_of_invalid + prev_time_sum_of_link_lost

                    curr_time_error = new_time_counters["error"]
                    curr_time_sum_of_invalid = new_time_counters["sum_of_invalid"]
                    curr_time_disconnected = new_time_counters["disconnected"]
                    curr_time_sum_of_link_lost = new_time_counters["sum_of_link_lost"]
                    curr_time_exception_sum = curr_time_error + curr_time_sum_of_invalid + curr_time_sum_of_link_lost + curr_time_disconnected
                    curr_time_counters_total = curr_time_counters_total  + curr_time_exception_sum

                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["error"] = curr_time_error
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["disconnected"] = curr_time_disconnected
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["sum_of_invalid"] = curr_time_sum_of_invalid
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["sum_of_link_lost"] = curr_time_sum_of_link_lost
      
                    increment_time_exception_sum = utils.calculate_increment(prev_time_exception,curr_time_exception_sum)
                    dashboards.gauge_acc_tts_counter_exception.labels(partitions_names[i]).set(curr_time_exception_sum)
                    dashboards.guage_inc_acc_tts_counter_exception.labels(partitions_names[i]).set(increment_time_exception_sum)
                    
                    prev_time_total = partitions_dict["tts-counters"][lpm][partitions_names[i]]["total"]
                    
                    partitions_dict["tts-counters"][lpm][partitions_names[i]]["total"] = curr_time_counters_total
                    dashboards.gauge_acc_tts_counter_total.labels(partitions_names[i]).set(curr_time_counters_total)
                    dashboards.gauge_inc_acc_tts_counter_total.labels(partitions_names[i]).set(curr_time_counters_total-prev_time_total)
                    
                    # transition
                    prev_transition_error = partitions_dict["tts-counters"][s][partitions_names[i]]["error"]
                    prev_transition_sum_of_invalid = partitions_dict["tts-counters"][s][partitions_names[i]]["sum_of_invalid"]
                    prev_transition_disconnected = partitions_dict["tts-counters"][s][partitions_names[i]]["disconnected"]
                    prev_transition_sum_of_link_lost = partitions_dict["tts-counters"][s][partitions_names[i]]["sum_of_link_lost"]
                    prev_transition_exception_sum = prev_transition_error + prev_transition_disconnected + prev_transition_sum_of_invalid + prev_transition_sum_of_link_lost
                    
                    curr_transition_error = new_transition_counters["error"]
                    curr_transition_sum_of_invalid = new_transition_counters["sum_of_invalid"]
                    curr_transition_disconnected = new_transition_counters["disconnected"]
                    curr_transition_sum_of_link_lost = new_transition_counters["sum_of_link_lost"]
                    curr_transition_exception_sum = curr_transition_error + curr_transition_sum_of_invalid + curr_transition_sum_of_invalid + curr_transition_disconnected
                    curr_transition_counters_total = curr_transition_counters_total + curr_transition_exception_sum
                    
                    partitions_dict["tts-counters"][s][partitions_names[i]]["error"] = curr_transition_error
                    partitions_dict["tts-counters"][s][partitions_names[i]]["disconnected"] = curr_transition_disconnected
                    partitions_dict["tts-counters"][s][partitions_names[i]]["sum_of_invalid"] = curr_transition_sum_of_invalid
                    partitions_dict["tts-counters"][s][partitions_names[i]]["sum_of_link_lost"] = curr_transition_sum_of_link_lost
                    
                    increment_transition_exception_sum = utils.calculate_increment(prev_transition_exception_sum,curr_transition_exception_sum)
                    
                    dashboards.gauge_acc_tts_transition_counter_exception.labels(partitions_names[i]).set(curr_transition_exception_sum)
                    dashboards.gauge_inc_acc_tts_transition_counter_exception.labels(partitions_names[i]).set(increment_transition_exception_sum)
                    prev_transition_total = partitions_dict["tts-counters"][s][partitions_names[i]]["total"]
                    
                    partitions_dict["tts-counters"][s][partitions_names[i]]["total"] = curr_transition_counters_total
                    dashboards.gauge_acc_tts_transition_counter_total.labels(partitions_names[i]).set(curr_transition_counters_total)
                    dashboards.gauge_inc_acc_tts_transition_counter_total.labels(partitions_names[i]).set(curr_transition_counters_total-prev_transition_total)
        

    # Bgo counters
    # Bgo counters
    # TOD0 create a func to split the 
    bgo_counters = ["0 (LumiNibble)","1 (BC0)","2 (TestEnable)","5 (Resync)","6 (HardReset)","7 (EC0)","8 (OC0)","9 (Start)","10 (Stop)","11 (StartOfGap)","13 (WarningTestEnable)"]
    for bgo_counter in bgo_counters:
        dashboards.gauge_bgo_counter.labels(bgo_counter.split("(")[1].split(")")[0]).set(utils.get_bgo_counter(snapshot, bgo_counter))


    dashboards.gauge_bgo_counter.labels("StartOfGap").set(utils.get_bgo_counter(snapshot, "12"))
    for remaining_bgo_counter in range(14,64):
        dashboards.gauge_bgo_counter.labels(remaining_bgo_counter).set(utils.get_bgo_counter(snapshot, str(remaining_bgo_counter)))
    # TO BE DONE: wrap all this functionality for pm counters in one function an calling in it for each one
    # instead of statically creating the list, fetching from the json 
     #  PM COUNTERS
    list_of_counters = ["TTS","Trigger rules","Bunch-mask","ReTri","PM APVE","DAQ backpressure","Calibration sequence","Software pauses","Firmware pauses"]
    list_of_counters_labels = ["tts","trigger_rules","bunch_mask","re_tri","pm_apve","daq_backpressure","calibration_sequence","software_pauses","firmware_pauses",]
    for i in range(0,len(list_of_counters)):

        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"ready").set(utils.get_tts_time_counter_pm_ready(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"warning").set(utils.get_tts_time_counter_pm_warning(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"warning_back").set(utils.get_tts_time_counter_pm_warning_back(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"requests").set(utils.get_tts_time_counter_pm_requests(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"error").set(utils.get_tts_time_counter_pm_error(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"disconnected").set(utils.get_tts_time_counter_pm_ready(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"invalids").set(utils.get_tts_time_counter_pm_invalids(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"links_lost").set(utils.get_tts_time_counter_pm_links_lost(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"busy").set(utils.get_tts_time_counter_pm_busy(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))
        dashboards.gauge_tts_time_counter_pm_x.labels(list_of_counters_labels[i],"out_of_sync").set(utils.get_tts_time_counter_pm_out_of_sync(snapshot["itemset-tts-time-counters-pm"],list_of_counters[i]))

    tts_sum = utils.get_sum_tts_time_counters_pm("TTS",snapshot)[0]
    tts_sum_not_ready = utils.get_sum_tts_time_counters_pm("TTS", snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("tts").set(tts_sum - pm_counters_dict["TTS"]["prev_sum"] )
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("tts").set(tts_sum_not_ready - pm_counters_dict["TTS"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("tts").set(tts_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("tts").set(tts_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("tts").set(utils.get_tts_time_counter_pm_ready("TTS",snapshot))
    pm_counters_dict["TTS"]["prev_sum"] = tts_sum
    pm_counters_dict["TTS"]["prev_sum_not_ready"] = tts_sum_not_ready

#  ---------------------------------------------------------------------------
    trigger_rules_sum = utils.get_sum_tts_time_counters_pm("Trigger rules",  snapshot)[0]
    trigger_rules_sum_not_ready = utils.get_sum_tts_time_counters_pm("Trigger rules",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("trigger_rules").set(trigger_rules_sum - pm_counters_dict["Trigger rules"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("trigger_rules").set(trigger_rules_sum_not_ready  - pm_counters_dict["Trigger rules"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("trigger_rules").set(trigger_rules_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("trigger_rules").set(trigger_rules_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("trigger_rules").set(utils.get_tts_time_counter_pm_ready("Trigger rules",snapshot))


    pm_counters_dict["Trigger rules"]["prev_sum"] = trigger_rules_sum
    pm_counters_dict["Trigger rules"]["prev_sum_not_ready"] = trigger_rules_sum_not_ready

# -------------------------------------------------------------------------------------------
    bunch_mask_sum = utils.get_sum_tts_time_counters_pm("Bunch-mask",  snapshot)[0]
    bunch_mask_sum_not_ready = utils.get_sum_tts_time_counters_pm("Bunch-mask",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("bunch_mask").set(bunch_mask_sum - pm_counters_dict["Bunch-mask"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("bunch_mask").set(bunch_mask_sum_not_ready -pm_counters_dict["Bunch-mask"]["prev_sum_not_ready"])
    dashboards.gauge_tts_time_counter_pm_sum.labels("bunch_mask").set(bunch_mask_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("bunch_mask").set(bunch_mask_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("bunch_mask").set(utils.get_tts_time_counter_pm_ready("Bunch-mask",snapshot))

    pm_counters_dict["Bunch-mask"]["prev_sum"] = bunch_mask_sum
    pm_counters_dict["Bunch-mask"]["prev_sum_not_ready"] = bunch_mask_sum_not_ready
#  ------------------------------------------------------------------------------------------
    re_tri_sum = utils.get_sum_tts_time_counters_pm("ReTri",  snapshot)[0]
    re_tri_sum_not_ready = utils.get_sum_tts_time_counters_pm("ReTri",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("re_tri").set(re_tri_sum - pm_counters_dict["ReTri"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("re_tri").set(re_tri_sum_not_ready - pm_counters_dict["ReTri"]["prev_sum_not_ready"])
    dashboards.gauge_tts_time_counter_pm_sum.labels("re_tri").set(re_tri_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("re_tri").set(re_tri_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("re_tri").set(utils.get_tts_time_counter_pm_ready("ReTri",snapshot))
    

    pm_counters_dict["ReTri"]["prev_sum"] = re_tri_sum
    pm_counters_dict["ReTri"]["prev_sum_not_ready"] = re_tri_sum_not_ready
# -------------------------------------------------------------------------------------------- 
    pm_apve_sum = utils.get_sum_tts_time_counters_pm("PM APVE",  snapshot)[0]
    pm_apve_sum_not_ready = utils.get_sum_tts_time_counters_pm("PM APVE",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("pm_apve").set(pm_apve_sum - pm_counters_dict["PM APVE"]["prev_sum"] )
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("pm_apve").set(pm_apve_sum_not_ready - pm_counters_dict["PM APVE"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("pm_apve").set(pm_apve_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("pm_apve").set(pm_apve_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("pm_apve").set(utils.get_tts_time_counter_pm_ready("PM APVE",snapshot))

    pm_counters_dict["PM APVE"]["prev_sum"] = pm_apve_sum
    pm_counters_dict["PM APVE"]["prev_sum_not_ready"] = pm_apve_sum_not_ready

# ---------------------------------------------------------------------------------------------------
    daq_backpressure_sum  = utils.get_sum_tts_time_counters_pm("DAQ backpressure",snapshot)[0]
    daq_backpressure_sum_not_ready = utils.get_sum_tts_time_counters_pm("DAQ backpressure",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("daq_backpressure").set(daq_backpressure_sum - pm_counters_dict["DAQ backpressure"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("daq_backpressure").set(daq_backpressure_sum_not_ready - pm_counters_dict["DAQ backpressure"]["prev_sum_not_ready"])
    dashboards.gauge_tts_time_counter_pm_sum.labels("daq_backpressure").set(daq_backpressure_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("daq_backpressure").set(daq_backpressure_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("daq_backpressure").set(utils.get_tts_time_counter_pm_ready("DAQ backpressure",snapshot))


    pm_counters_dict["DAQ backpressure"]["prev_sum"] = daq_backpressure_sum
    pm_counters_dict["DAQ backpressure"]["prev_sum_not_ready"] = daq_backpressure_sum_not_ready

#  -----------------------------------------------------------------------------------------------------

    calibration_sequence_sum = utils.get_sum_tts_time_counters_pm(
        "Calibration sequence",  snapshot)[0]
    calibration_sequence_sum_not_ready = utils.get_sum_tts_time_counters_pm(
        "Calibration sequence",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("calibration_sequence").set(calibration_sequence_sum - pm_counters_dict["Calibration sequence"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("calibration_sequence").set(calibration_sequence_sum_not_ready - pm_counters_dict["Calibration sequence"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("calibration_sequence").set(calibration_sequence_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("calibration_sequence").set(calibration_sequence_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("calibration_sequence").set(utils.get_tts_time_counter_pm_ready("Calibration sequence",snapshot))

    pm_counters_dict["Calibration sequence"]["prev_sum"] = calibration_sequence_sum
    pm_counters_dict["Calibration sequence"]["prev_sum_not_ready"] = calibration_sequence_sum_not_ready
# -----------------------------------------------------------------------------------------------------------
    software_pauses_sum = utils.get_sum_tts_time_counters_pm("Software pauses",  snapshot)[0]
    software_pauses_sum_not_ready = utils.get_sum_tts_time_counters_pm("Software pauses",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("software_pauses").set(software_pauses_sum - pm_counters_dict["Software pauses"]["prev_sum"])
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("software_pauses").set(software_pauses_sum_not_ready -pm_counters_dict["Software pauses"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("software_pauses").set(software_pauses_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("software_pauses").set(software_pauses_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("software_pauses").set(utils.get_tts_time_counter_pm_ready("Software pauses",snapshot))


    pm_counters_dict["Software pauses"]["prev_sum"] = software_pauses_sum
    pm_counters_dict["Software pauses"]["prev_sum_not_ready"] = software_pauses_sum_not_ready
# -----------------------------------------------------------------------------------------------------------------

    firmware_pauses_sum = utils.get_sum_tts_time_counters_pm("Firmware pauses",  snapshot)[0]
    firmware_pauses_sum_not_ready = utils.get_sum_tts_time_counters_pm("Firmware pauses",  snapshot)[1]

    dashboards.gauge_tts_time_counter_pm_inc_sum.labels("firmware_pauses").set(firmware_pauses_sum - pm_counters_dict["Firmware pauses"]["prev_sum"] )
    dashboards.gauge_tts_time_counter_pm_inc_not_ready.labels("firmware_pauses").set(firmware_pauses_sum_not_ready -pm_counters_dict["Firmware pauses"]["prev_sum_not_ready"] )
    dashboards.gauge_tts_time_counter_pm_sum.labels("firmware_pauses").set(firmware_pauses_sum)
    dashboards.gauge_tts_time_counter_pm_not_ready.labels("firmware_pauses").set(firmware_pauses_sum_not_ready)
    # dashboards.gauge_tts_time_counter_pm_ready.labels("firmware_pauses").set(utils.get_tts_time_counter_pm_ready("Firmware pauses",snapshot))


    pm_counters_dict["Firmware pauses"]["prev_sum"] = firmware_pauses_sum
    pm_counters_dict["Firmware pauses"]["prev_sum_not_ready"] = firmware_pauses_sum_not_ready


    
if __name__ == "__main__":
    
    start_http_server(8001)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    # dummy date to compare for the first time
    last_procced_snapshot = datetime.datetime(1970, 1, 1)


    response = urllib.request.urlopen(url)
    snapshot = json.loads(response.read())
    run_number = snapshot["Run configuration"]["Run number"]

    partitions_dict = (utils.create_partitions_dict(snapshot))[0]
    partitions_index_dict = (
        utils.create_partitions_dict(snapshot))[1]

    
    
    pm_counters_dict = utils.create_pm_counters_dict(snapshot)
   

    while not interrupted:
        try:

            start_time = datetime.datetime.now()
            response = urllib.request.urlopen(url)
            snapshot = json.loads(response.read())

            if interrupted == True:
                break

        except HTTPError as e:
            print('Error code: ', e.code)
        except URLError as e:
            print('Reason: ', e.reason)
        run_number = snapshot["Run configuration"]["Run number"]
        # print(run_number)

        if run_number != last_run_number:
            logging.info(f"new_run_number: {run_number}, old_run_number: {last_run_number}")
       

        snapshot_timestamp = datetime.datetime.strptime(
            snapshot["Application state"]["Latest monitoring update time"].split(" UTC")[0], "%Y-%m-%d %H:%M:%S")

        if last_procced_snapshot >= snapshot_timestamp:
            # print("Got the same snapshot again! Not processing..")
            time.sleep(poll_time_seconds)
            dashboards.gauge_not_new_snapshot.set(not_new_snapshot)
            not_new_snapshot +=1
            continue

            # for i in range(0,len(all_partitions)):
            #     dashboards.gauge_acc_tts_counter_ready.labels(all_partitions[i]).set(-1)
            #     dashboards.gauge_acc_tts_counter_warning.labels(all_partitions[i]).set(-1)
            #     dashboards.gauge_acc_tts_counter_warning_back.labels(all_partitions[i]).set(-1)
            #     dashboards.gauge_acc_tts_counter_warning.out_of_sync(all_partitions[i]).set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("tts").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("tts").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("trigger_rules").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("trigger_rules").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("bunch_mask").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("bunch_mask").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("re_tri").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("re_tri").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("pm_apve").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("pm_apve").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("daq_backpressure").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("daq_backpressure").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("calibration_sequence").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("calibration_sequence").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("software_pauses").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("software_pauses").set(-1)

            # dashboards.gauge_tts_time_counter_pm_sum.labels("firmware_pauses").set(-1)
            # dashboards.gauge_tts_time_counter_pm_not_ready.labels("firmware_pauses").set(-1)
            # time.sleep(poll_time_seconds)

        # print("Found new snapshot: ", snapshot_timestamp)
        last_procced_snapshot = snapshot_timestamp
        work_snapshot(snapshot, partitions_dict, partitions_index_dict,pm_counters_dict)
      
        
        processing_time_seconds = (
            datetime.datetime.now() - start_time).total_seconds()

        if processing_time_seconds < poll_time_seconds:
            time.sleep(poll_time_seconds - processing_time_seconds)
