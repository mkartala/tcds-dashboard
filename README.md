# tcds-dashboard

CAUTION! 

Always remember to terminate the script using SIGINT which is ctrl+c, and if it is running in the background with nohup, find the PID of the process and use the kill command with -2 as argument which is the SIGINT

Using the default signal , SIGKILL can cause bad exit of the programm and in addition the CPM application has happened to wait 
for the script to consume the data.