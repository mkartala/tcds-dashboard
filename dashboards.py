from prometheus_client import Gauge


gauge_tts_time_counter_pm_sum = Gauge(
    "tts_time_counter_sum", "TTS time counter", ["counter_name"])
gauge_tts_time_counter_pm_inc_sum = Gauge("tts_time_counter_inc_sum","The incerement of the sum for tts time counter",["counter_name"])

gauge_tts_time_counter_pm_not_ready = Gauge(
    "tts_time_counter_not_ready", "TTS time counter", ["counter_name"])
gauge_tts_time_counter_pm_inc_not_ready = Gauge("tts_time_counter_inc_not_ready", "The increment of the sum - not ready for tts time counter",["counter_name"])

gauge_tts_time_counter_pm_x = Gauge("tts_time_counter_pm_x","This metric stores all the different counters inside each time-counter-pm",["counter_name","counter_type"])



# names should change to acc_tts_time_counters_lpm
gauge_acc_tts_counter_ready = Gauge("acc_tts_counter_ready", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_ready = Gauge("inc_acc_tts_counter_ready", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_warning = Gauge("acc_tts_counter_warning", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_warning = Gauge("inc_acc_tts_counter_warning", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_warning_back = Gauge("acc_tts_counter_warning_back", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_warning_back = Gauge("inc_acc_tts_counter_warning_back", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_busy = Gauge("acc_tts_counter_busy", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_busy = Gauge("inc_acc_tts_counter_busy", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_out_of_sync = Gauge("acc_tts_counter_out_of_sync", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_out_of_sync = Gauge("inc_acc_tts_counter_out_of_sync", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_private_requests = Gauge("acc_tts_counter_private_request", "TTs time counters", ["partition_name"])
gauge_inc_acc_tts_counter_private_requests = Gauge("inc_acc_tts_counter_private_requests", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_exception = Gauge("acc_tts_counter_exception", "TTs time counters", ["partition_name"])
guage_inc_acc_tts_counter_exception = Gauge("inc_acc_tts_counter_exception", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_total = Gauge("acc_tts_counter_total","TTs time counters",["partition_name"])
gauge_inc_acc_tts_counter_total = Gauge("inc_acc_tts_counter_total", "TTs time counters", ["partition_name"])
gauge_acc_tts_counter_ready_and_warning = Gauge("acc_tts_counter_ready_and_warning", "TTs time counters", ["partition_name"])
 
# ------------------------- itemset-tts-transition-counters-lpm-------------------------------------

gauge_acc_tts_transition_counter_ready = Gauge("acc_tts_transition_counter_ready","tts transition counter",["partition_name"])
gauge_inc_acc_tts_transition_counter_ready = Gauge("inc_acc_tts_transition_counter_ready", "tts transition counter", ["partition_name"])
gauge_acc_tts_transition_counter_warning = Gauge("acc_tts_transition_counter_warning", "tts transition counters", ["partition_name"])
gauge_inc_acc_tts_transition_counter_warning = Gauge("inc_acc_tts_transition_counter_warning","tts transition counters", ["partition_name"])
gauge_acc_tts_transition_counter_warning_back = Gauge("acc_tts_transition_counter_warning_back","tts transition counters",["partition_name"])
gauge_inc_acc_tts_transition_counter_warning_back = Gauge("inc_acc_tts_transition_counter_warning_back","tts transition counters",["partition_name"])
gauge_acc_tts_transition_counter_busy = Gauge("acc_tts_transition_counter_busy","tts transition counters",["partition_name"])
gauge_inc_acc_tts_transition_counter_busy = Gauge("inc_acc_tts_transition_counter_busy","tts transition counters",["partition_name"])
gauge_acc_tts_transition_counter_out_of_sync = Gauge("acc_tts_transition_counter_out_of_sync","tts transition counters",["partition_name"])
gauge_inc_acc_tts_transition_counter_out_of_sync = Gauge("inc_acc_tts_transition_counter_out_of_sync","tts transition counters",["partition_name"])
gauge_acc_tts_transition_counter_private_requests = Gauge("acc_tts_transition_counter_private_requests","tts transition counters",["partition_name"])
gauge_inc_acc_tts_transition_counter_private_requests = Gauge("inc_acc_tts_transition_counter_private_requests","tts transition counters",["partition_name"])
gauge_acc_tts_transition_counter_exception = Gauge("acc_tts_transition_counter_exception","tts transition counters",["partition_name"])
gauge_inc_acc_tts_transition_counter_exception = Gauge("inc_acc_tts_transition_counter_exception","tts transition counters",["partition_name"])
gauge_acc_tts_transition_counter_total = Gauge("acc_tts_transition_counter_total","tts transition counter", ["partition_name"])
gauge_inc_acc_tts_transition_counter_total = Gauge("inc_acc_tts_transition_counter_total","tts transition counter", ["partition_name"])
gauge_acc_tts_transition_counter_ready_and_warning = Gauge("acc_tts_transition_counter_ready_and_warning","tts transition counter",["partition_name"])

# ---------------------------------------------------------------------------------------------------------
gauge_not_new_snapshot = Gauge("not_new_snapshot","This metric is set with a value when I dont get new snapshot")
gauge_latest_monitoring_update_time = Gauge("latest_monitoring_update_time","The latest update")
gauge_latest_monitoring_update_duration = Gauge("latest_monitoring_update_duration", "The duration of the latest update")
gauge_total_L1As_in_current_run = Gauge("total_L1As_in_current_run","The total L1As in the current run")

gauge_nibbles_rates_total_trigger_rate = Gauge("nibbles_rates_total_trigger_rate","The total triger rate")
gauge_nibbles_rates_physics_trigger = Gauge("nibbles_rates_physics_trigger", "The physics trigger")
gauge_nibbles_rates_sequence_calibration_trigger = Gauge("nibbles_rates_sequence_calibration_trigger","The sequence calibration trigger")
gauge_nibbles_rates_random_trigger = Gauge("nibbles_rates_random_trigger", "The random trigger")


gauge_nibbles_deadtimes_total = Gauge("nibbles_deadtime_total","This is the total of the deadtimes")
gauge_nibbles_deadtime_counter = Gauge("nibbles_deadtime_counter","This is a metric for each deadtime with a label the type",["due_to"])


gauge_latest_section_lumi_section_number = Gauge("lumi_section_number","Then number of lumi section")
gauge_latest_section_number_of_nibbles= Gauge("latest_section_number_of_nibbles","The number of nibbles in the last section")

gauge_bgo_counter = Gauge("bgo_counter","The bgo counter with label",["explanation"])