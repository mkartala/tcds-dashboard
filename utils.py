import urllib3
import json
http = urllib3.PoolManager()
import dashboards
response = ""

# THINGS_TODO in some of the functions name snapshot is misleading since it is part of the snapshot


def get_sum_tts_time_counters_pm(counter_name,snapshot) :
    """
    This function loop is developer for itemset-tts-time-counters-pm
    It takes the sum for each one (15 differnets counters) and the 
    sum of not ready
    """
    dict_keys = list(snapshot["itemset-tts-time-counters-pm"])
    sum = 0
    for i in range(len(dict_keys)):
        if counter_name in dict_keys[i]:
            sum += int(snapshot["itemset-tts-time-counters-pm"][dict_keys[i]])
    return sum, sum - int(snapshot["itemset-tts-time-counters-pm"][counter_name+";ready"])


def get_tts_time_counter_pm_ready(itemset_tts_time_counters_pm,counter_name):
    
    return int(itemset_tts_time_counters_pm[counter_name+";ready"])

def get_tts_time_counter_pm_busy(itemset_tts_time_counters_pm,counter_name):
    
    return int(itemset_tts_time_counters_pm[counter_name+";busy"])

def get_tts_time_counter_pm_out_of_sync(itemset_tts_time_counters_pm,counter_name):
    
    return int(itemset_tts_time_counters_pm[counter_name+";out-of-sync"])

def get_tts_time_counter_pm_warning(itemset_tts_time_counters_pm,counter_name):    
    return int(itemset_tts_time_counters_pm[counter_name+";warning"])

def get_tts_time_counter_pm_warning_back(itemset_tts_time_counters_pm,counter_name):    
    return int(itemset_tts_time_counters_pm[counter_name+";warning (backpressured)"])

def get_tts_time_counter_pm_requests(itemset_tts_time_counters_pm,counter_name):
    return (int(itemset_tts_time_counters_pm[counter_name+";"+"private request 1"]) + int(itemset_tts_time_counters_pm[counter_name+";"+"private request 2"]) +int(itemset_tts_time_counters_pm[counter_name+";"+"private request 3"]))

def get_tts_time_counter_pm_error(itemset_tts_time_counters_pm,counter_name):
    return (int(itemset_tts_time_counters_pm[counter_name+";"+"error"]))

def get_tts_time_counter_pm_disconnected(itemset_tts_time_counters_pm,counter_name):
    return (int(itemset_tts_time_counters_pm[counter_name+";"+"disconnected (0x00)"]))

def get_tts_time_counter_pm_invalids(itemset_tts_time_counters_pm,counter_name):

    keys = itemset_tts_time_counters_pm.keys()

    filter_string = counter_name + ";"+ "invalid"
    dict_keys = (list(filter(lambda x:filter_string in x,keys)))

    sum_of_invalid = 0 
    for dict_key in dict_keys:
        sum_of_invalid += int(itemset_tts_time_counters_pm[dict_key])

    return sum_of_invalid

def get_tts_time_counter_pm_links_lost(itemset_tts_time_counters_pm,counter_name):

    keys = itemset_tts_time_counters_pm.keys()

    filter_string = counter_name + ";"+ "link lost"
    dict_keys = (list(filter(lambda x:filter_string in x,keys)))

    sum_of_links_lost = 0 
    for dict_key in dict_keys:
        sum_of_links_lost += int(itemset_tts_time_counters_pm[dict_key])

    return sum_of_links_lost
 
def create_pm_counters_dict(snapshot):

    pm_counters_dict = dict
    pm_counters_set = set()
    for item in snapshot["itemset-tts-time-counters-pm"]:
        # print(item.split(';')[0])
        pm_counters_set.add(item.split(";")[0])

    # pm_counters_dict = dict.fromkeys(
    #     pm_counters_set, {"prev_sum": 0, "prev_sum_not_ready": 0})
    pm_counters_dict = dict.fromkeys(pm_counters_set)
    
    for key in pm_counters_dict:
        pm_counters_dict[key] = {"prev_sum":0,"prev_sum_not_ready":0}

    return (pm_counters_dict)

def create_partitions_dict(snapshot):
    """
    This function creates a dictionary where the first key is the tts-counters with values the
    itemset-tts-time-counters-lpmX . It goes first to the itemset-tts-state so to get the partitions that
    are in the run (which means if the value !=masked). Then for each partition it creates a nested dictionary
    with the keys we are interested in storing. Key requests is the sum of the private requests the same
    applies for the link lost and invalid

    In addition, when it gets a partition name it also stores in a second dictionary the index that it was found,
    for example if it found ICI1 BPIX- it will keep the 1 as a value in a dictionary with key the partition name
    In this way it is easy for the getters functions to build the string in order to retrieve the value from the snapshots
    """

    partition_dict = dict()
    partitions_index_dict = dict()

    partition_dict.update({"tts-counters": {}})
    states_list = get_lpms_state(snapshot)
# looping for each state of the lpms
    for state in states_list:

        partition_dict["tts-counters"].update(
            {"itemset-tts-time-counters-"+state.split("-")[3]: {}})
        partition_dict["tts-counters"].update(
            {"itemset-tts-transition-counters-"+state.split("-")[3]: {}})
        # checking if a partition is in the run
        for partition_name in snapshot[state]:
            if "APVE" not in partition_name and "(Unused)" not in partition_name:

                partition_dict["tts-counters"]["itemset-tts-time-counters-"+state.split("-")[3]].update({partition_name.split("(")[1].split(")")[0]: {"ready": 0, "warning": 0, "warning_back": 0,
                                                                                                                                                      "busy": 0, "out_of_sync": 0, "disconnected": 0, "requests": 0,  "error": 0, "sum_of_invalid": 0, "sum_of_link_lost": 0, "total": 0}})
                partition_dict["tts-counters"]["itemset-tts-transition-counters-"+state.split("-")[3]].update({partition_name.split("(")[1].split(")")[0]: {"ready": 0, "warning": 0, "warning_back": 0,
                                                                                                                                                      "busy": 0, "out_of_sync": 0, "disconnected": 0, "requests": 0,  "error": 0, "sum_of_invalid": 0, "sum_of_link_lost": 0, "total": 0}})
                partitions_index_dict.update({partition_name.split("(")[1].split(")")[
                    0]: (partition_name.split("(")[0].split("ICI")[1]).strip()})

    return partition_dict, partitions_index_dict


def get_lpms_state(snapshot):    
    """
    This function returns a list with all the lpms, currently there are 12
    it is used in order to loop trough them and get the partitiions names
    """

    keys = snapshot.keys()
    return (
        list(filter(lambda x: "itemset-tts-states-lpm" in x, keys)))


def get_lpms(snapshot):
    """
    This function returns a list with all the lpms, currently there are 12, they are used later
    to build the dictionary. so after this function is called the format of the dictionary looks
    like "tts-counters":{itemset-tts-time-counters-lpmX : }
    """
    keys = snapshot.keys()
    return (
        list(filter(lambda x: "itemset-tts-time-counters-lpm" in x, keys)))


def  get_tts_time_and_transition_counter_lpm_all_values(snapshot,lpm,partition_name,index):
    # todo: add a falg to check the tts-state only the begin of a new run
    
    """
    This function is checking if the partition is in the run and if it is she calls all the other functions to fetch 
    the values and return them in a dictionary . For each partition we have to know to which lpm it is. If the the partition
    is masked it just return the string "not_in_the_run". So always check the return type when using it
    """

    time_counters = {"ready": 0, "warning": 0, "warning_back": 0, "busy": 0, "out_of_sync": 0, "disconnected": 0, "requests": 0,  "error": 0, "sum_of_invalid": 0, "sum_of_link_lost": 0}
    transition_counters = {"ready": 0, "warning": 0, "warning_back": 0, "busy": 0, "out_of_sync": 0, "disconnected": 0, "requests": 0,  "error": 0, "sum_of_invalid": 0, "sum_of_link_lost": 0}
    s = lpm.replace("time","transition")

    if snapshot["itemset-tts-states-"+lpm.split("-")[4]]["ICI"+index+" " + "("+partition_name+")"] != "masked":
        
        time_counters["ready"] = get_tts_time_counter_lpm_ready(snapshot[lpm],partition_name,index)
        time_counters["warning"] = get_tts_time_counter_lpm_warning(snapshot[lpm],partition_name,index)
        time_counters["warning_back"] = get_tts_time_counter_lpm_warning_back(snapshot[lpm],partition_name,index)
        time_counters["busy"] = get_tts_time_counter_lpm_busy(snapshot[lpm],partition_name,index)
        time_counters["out_of_sync"] = get_tts_time_counter_lpm_out_of_sync(snapshot[lpm],partition_name,index)
        time_counters["disconnected"] = get_tts_time_counter_lpm_disconnected(snapshot[lpm],partition_name,index)
        time_counters["requests"] = get_tts_time_counter_lpm_private_requests(snapshot[lpm],partition_name,index)
        time_counters["error"] = get_tts_time_counter_lpm_error(snapshot[lpm],partition_name,index)
        time_counters["sum_of_invalid"] = get_tts_time_counter_lpm_sum_invalid(snapshot[lpm],partition_name,index)
        time_counters["sum_of_link_lost"] = get_tts_time_counter_lpm_sum_invalid(snapshot[lpm],partition_name,index)
        
        transition_counters["ready"] = get_tts_transition_counter_lpm_ready(snapshot[s],partition_name,index)
        transition_counters["warning"] = get_tts_transition_counter_lpm_warning(snapshot[s],partition_name,index)
        transition_counters["warning_back"] = get_tts_transition_counter_lpm_warning_back(snapshot[s],partition_name,index)
        transition_counters["busy"] = get_tts_transition_counter_lpm_busy(snapshot[s],partition_name,index)
        transition_counters["out_of_sync"] = get_tts_transition_counter_lpm_out_of_sync(snapshot[s],partition_name,index)
        transition_counters["disconnected"] = get_tts_transition_counter_lpm_disconnected(snapshot[s],partition_name,index)
        transition_counters["requests"] = get_tts_transition_counter_lpm_private_requests(snapshot[s],partition_name,index)
        transition_counters["error"] = get_tts_transition_counter_lpm_error(snapshot[s],partition_name,index)
        transition_counters["sum_of_invalid"] = get_tts_transition_counter_lpm_sum_invalid(snapshot[s],partition_name,index)
        transition_counters["sum_of_link_lost"] = get_tts_transition_counter_lpm_sum_invalid(snapshot[s],partition_name,index)
    else:
        # todo refactor the return types and the check in the function call
        return "not_in_the_run","not_in_the_run"

    return time_counters,transition_counters
        

def calculate_increment(prev_value,curr_value):

    if prev_value == -1:
        increment_of_the_value = 0
    else:
        increment_of_the_value = curr_value - prev_value

    return increment_of_the_value  



def get_tts_time_counter_lpm_ready(lpm, partition_name, index):
    """
    This function returns the value of the ready counter for a given partition
    """

    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";ready"])


def get_tts_time_counter_lpm_warning(lpm, partition_name, index):
    """
    This function returns the value of the warning counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";warning"])


def get_tts_time_counter_lpm_warning_back(lpm, partition_name, index):
    """
    This function returns the value of the warning counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";warning (backpressured)"])


def get_tts_time_counter_lpm_out_of_sync(lpm, partition_name, index):
    """
    This function returns the value of the out of sync counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";out-of-sync"])


def get_tts_time_counter_lpm_busy(lpm, partition_name, index):
    """
    This function returns the value of the busy counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";busy"])


def get_tts_time_counter_lpm_error(lpm, partition_name, index):
    """
    This function returns the value of the error counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";error"])


def get_tts_time_counter_lpm_disconnected(lpm, partition_name, index):
    """
    This function returns the value of the disconnected counter for a given partition
    """
    return int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";disconnected (0x00)"])


def get_tts_time_counter_lpm_sum_invalid(lpm, partition_name, index):
    """
    This function returns the value of the invalid counter for a given partition
    It firsts gets all the keys that may include the world invalid, currently there are 2
    (FED,0x03), (TCDS) and
    """
    keys = lpm.keys()
    filter_string = "ICI" + \
        str(index)+" "+"("+partition_name + ")" + ";" + "invalid"
    dict_keys = (list(filter(lambda x: filter_string in x, keys)))

    sum_of_invalid = 0
    for dict_key in dict_keys:
        sum_of_invalid += int(lpm[dict_key])
    return sum_of_invalid


def get_tts_time_counter_lpm_sum_link_lost(lpm, partition_name, index):
    """
    This function returns the sum  of the link lost values  for a given partition
    Usually there are three PI,LPM,CPM
    """
    keys = lpm.keys()

    filter_string = "ICI" + \
        str(index)+" "+"("+partition_name + ")" + ";" + "link lost"

    dict_keys = (list(filter(lambda x: filter_string in x, keys)))

    sum_of_link_lost = 0

    for dict_key in dict_keys:
        sum_of_link_lost += int((lpm[dict_key]))

    return sum_of_link_lost


def get_tts_time_counter_lpm_private_requests(lpm, partition_name, index):
    """
    This function returns the private requests which is the sym of all the requests for a given partition
    """
    return (int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 1"]) + int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 2"]) + int(lpm["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 3"]))

def get_tts_transition_counter_lpm_ready(s,partition_name,index):
 
    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";ready"])

def get_tts_transition_counter_lpm_warning(s,partition_name,index):
    
    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";warning"])

def get_tts_transition_counter_lpm_warning_back(s,partition_name,index):

  
    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";warning (backpressured)"])

def get_tts_transition_counter_lpm_out_of_sync(s,partition_name,index):

    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";out-of-sync"])

def get_tts_transition_counter_lpm_busy(s,partition_name,index):
    
    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";busy"])

def get_tts_transition_counter_lpm_disconnected(s,partition_name,index):
    
    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";disconnected (0x00)"])

def get_tts_transition_counter_lpm_error(s, partition_name, index):

    return int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";error"])

def get_tts_transition_counter_lpm_sum_invalid(s,partition_name,index):

    keys = s.keys()
    filter_string = "ICI" + \
        str(index)+" "+"("+partition_name + ")" + ";" + "invalid"
    dict_keys = (list(filter(lambda x: filter_string in x, keys)))
    sum_of_invalid = 0
    for dict_key in dict_keys:
        sum_of_invalid += int(s[dict_key])
    return sum_of_invalid
def get_tts_transition_counter_lpm_sum_link_lost(s,partition_name,index):
   
    filter_string = "ICI" + \
        str(index)+" "+"("+partition_name + ")" + ";" + "link lost"

    dict_keys = (list(filter(lambda x: filter_string in x, keys)))

    sum_of_link_lost = 0

    for dict_key in dict_keys:
        sum_of_link_lost += int((s[dict_key]))

    return sum_of_link_lost
def get_tts_transition_counter_lpm_private_requests(s,partition_name,index):

    return (int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 1"]) + int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 2"]) + int(s["ICI"+str(index)+" " + "("+partition_name+")" + ";private request 3"]))


def get_nibbles_rate(snapshot,rate_name):
    
    rate = snapshot["itemset-latest-nibbles-rates-plain"][rate_name]
    
    if rate.isnumeric() == False:
        return -1
    else:
        return float(rate)

def get_nibbles_deadtimes(snapshot,dead_time):

    dead_time = snapshot["itemset-latest-nibbles-deadtimes-plain"][dead_time]

    if dead_time == "-":
        return -1
    else:
        return float(dead_time)

def get_cpm_latest_section_value(snapshot,value_name):
    
    value = snapshot["itemset-cpm-latest-section"][value_name]

    if value.isnumeric() == False:
        return -1
    else:
        return float(value)

def get_trigger_counter_value(snapshot,value_name):
    
    value = snapshot["itemset-trigger-counter"][value_name]

    if value.isnumeric() == False:
        return -1
    else:
        return float(value)

def get_bgo_counter(snapshot, counter_name):
    """
    This function return the value of the given bgo counter

    """
    value = snapshot["itemset-bgo-counters"]["B-go "+counter_name]

    if value.isnumeric() == False:
        return -1
    else:
        return int(snapshot["itemset-bgo-counters"]["B-go "+counter_name])

def make_post_request(encoded_body, url, token):
    global response
    """
    This function create a post_request to grafana annotations api
    It needs the body of the request encoded as json, where time and timeEnd  arein milleseconds
    """
    http = urllib3.PoolManager()

    response = http.request("POST", url, headers={"Content-Type": "application/json", "Authorization": token, "Accept": "application/json"},
                            body=encoded_body)

    if response.status != 200:
        print("Annotation POST request failed: ")
        print(json.dumps(json.loads(response.data.decode("utf-8")), indent=2))
    else:
        return "Annotation POST request succeed"


def create_encoded_body(method: str, dashboardUID: str, panelId: int, tags="", description=" ", time: int = 0, timeEnd: int = 0):
    """
    This function create the body of the request you ara going to make. Urrlib3 library need the 
    body of the requests formated as json. This function dependind on the method of the requests 
    creates the correct encoded_body
    """

    if method == "POST":
        if timeEnd == 0:
            timeEnd = time
        return json.dumps({"dashboardUID": dashboardUID, "panelId": panelId, "time": time, "timeEnd": timeEnd, "tags": [tags], "text": description})

    elif method == "GET":
        return "GET request does not need a encoded_body :) "

    elif method == "DELETE":
        return json.dumps({"dasboardUID": dashboardUID, "panelId": panelId})

    # to be implemented
    elif method == "PATCH":
        pass

    return "No other request method supported"
